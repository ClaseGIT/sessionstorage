function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
}
function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor1 = document.getElementById("spanValor1");
  spanValor1.innerText = "El elemento es: " + valor;
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function eliminarDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.removeItem(clave);
  spanValor2.innerText = "El elemento " + clave + " ha sido eliminado";
}	

function borrarDeSessionStorage() {
  sessionStorage.clear();	
  var spanValor3 = document.getElementById("spanValor3");
  spanValor3.innerText = "Se borraron los elementos del Session Storage";
}

function longitudEnSessionStorage() {
  var numElementos = sessionStorage.length;
  var spanValor4 = document.getElementById("spanValor4");
  spanValor4.innerText = "El número guardado de elementos es: " + numElementos;
}